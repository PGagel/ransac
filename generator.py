#!/usr/bin/python3

import sys as sys
import csv as csv
import random as rnd

class Line:
    def __init__(self, a:float, b:float):
        self.a = a
        self.b = b
    def __repr__(self):
        return "Line: " + repr(self.a) + "x + " + repr(self.b)

noise_level = 2
domain = range(0, 10)
points_num = 100
line = Line(float('NaN'), float('NaN'))
output = "./result.csv"

def main(argv):
    error = interpret_input_args(argv)
    
    if error:
        return 1

    if line.a == float('NaN') or line.b == float('NaN'):
        print("No line generation possible. No line given. See -h for more information")
        return 1

    start = domain.start
    stop = domain.stop
    domain_size = abs(stop) - abs(start) * 1.0
    
    print("working with " + repr(line))

    with open(output, "w") as csv_file:
        writer = csv.writer(csv_file, delimiter=",")
        for i in range(0, points_num):
            noise_curr = rnd.random() * noise_level * 2 - noise_level
            x = rnd.random() * domain_size + start
            y = x * line.a + line.b + noise_curr
            point = [str(x), str(y)]
            writer.writerow(point)
    
    return 0

def interpret_input_args(argv):
    global line, points_num, output, noise_level
    global domain
    for i in range(0,len(argv)):
        if argv[i] == "--line" or argv[i] == "-l":
            parameters = argv[i+1].split('+')
            a = float(parameters[0][:len(parameters[0])-1])
            b =  float(parameters[1])
            line = Line(a, b)
            i = i+1
        elif argv[i] == "--random" or argv[i] == "-r":
            a = rnd.random() * 10 - 5
            b = rnd.random() * 10 - 5
            line = Line(a, b)
        elif argv[i] == "--domain" or argv[i] == "-d":
            domain_borders = argv[i+1].split(':')
            lower = int(domain_borders[0])
            upper = int(domain_borders[1])
            if lower >= upper:
                print("No valid domain delection. Please select domain like such: -range 1:10")
                return True;
            domain = range(lower, upper)
            i = i+1
        elif argv[i] == "--points-num" or argv[i] == "-p":
            points_num = int(argv[i+1])
            i = i+1
        elif argv[i] == "--noise" or argv[i] == "-n":
            noise_level = float(argv[i+1])
            i = i+1
        elif argv[i] == "--output" or argv[i] == "-o":
            output = argv[i+1]
            i = i+1
        elif argv[i] == "--help" or argv[i] == "-h":
            help();
            return True;

    return False

def help():
    print ("This script generates random points with noise around a given line.\nIt has the following parameters:")
    print ("Necessary parameters. Select one:")
    print()
    print ("--line | -l <line in the form ax+b>\t\t\t\t(no spaces between a, x, + and b)")
    print ("--random | -r <>\t\t\t\t\t\tGenerates a random line")
    print()    
    print ("Optional parameters. These have internal default values")
    print()
    print ("--domain | -d <domain in the form start:end> default: 1:10\t(no spaces between start, : and end)")
    print ("--points-num | -p <# of points to generate> default: 100")
    print ("--noise | -n <noise level> default: 2\t\t\t\tOne value that defines absolute boundaries in which the noise is generated.")
    print ("-- output | -o <path to ouput csv-file> default: ./ouput.csv")

if __name__ == "__main__":
    main(sys.argv[1:])
