#include <opencv2/imgproc.hpp>
#include "Ransac.hpp"


Ransac::Ransac(){
    guess_num = 1;
}

Line Ransac::approx(const vector<Point2d> points, int max_iter, double consens_edge, double accuracy, bool show_progress, int progress_delay){
    _points = points;
    Line line_best = {0, 0};
    double mse_best = numeric_limits<double>::max();
    accuracy /= 100;

    namedWindow("Guess");

    // init random generator
    random_device rd;
    mt19937 mt(rd());
    uniform_real_distribution<double> dist(0, _points.size());

    int i;
    for (i = 0; i < max_iter; i++) {
        // get rnd points
        auto idx1 = static_cast<unsigned long>(dist(mt));
        auto idx2 = static_cast<unsigned long>(dist(mt));

        // calculate line and get according confidant
        Line line_curr = get_line(_points.at(idx1), _points.at(idx2));
        Confidant conf_curr = get_confidant(_points, line_curr, consens_edge);


        // check if the new line fits better
        if (conf_curr.mse < mse_best) {
            line_best = line_curr;
            mse_best = conf_curr.mse;
        }

        // if the accuracy is reached, stop computation
        if(conf_curr.accur > accuracy) {
            cout << "Interrupted because accuracy of " << conf_curr.accur << " larger than expected accuracy of "<< accuracy << "." << endl;
            break;
        }

        if(show_progress) {
            show_line(line_curr, false);
            waitKey(static_cast<int>(chrono::operator""ms(progress_delay).count()));

            //this_thread::sleep_for(chrono::operator""ms(progress_delay));
        }

        guess_num++;
    }
    cout << "Finished calculation after: " << i << " iterations." << endl;

    destroyWindow("Guess");

    return line_best;
}


// ==== Implementation Helper Methods ==== //
double inline Ransac::get_y(Line line, double x){
    return x*line.a + line.b;
}

Line Ransac::get_line(Point2d a, Point2d b){
    Line line={0, 0};
    double dx = b.x - a.x;
    double dy = b.y - a.y;
    line.a = dy / dx;
    line.b = a.y - line.a*a.x;
    return line;
}

Confidant Ransac::get_confidant(vector<Point2d> cloud, Line line, double consens_edge){
    Confidant conf = {0, 0};
    // calculate Mean Error for cloud and approx
    for (Point2d point : cloud){
        double dist = abs(point.y - get_y(line, point.x));
        conf.mse += dist;
        if(dist <= consens_edge)
            conf.accur++;
    }
    // calculate results
    conf.mse /= cloud.size();
    conf.accur /= cloud.size();
    return conf;
}

void min_max_xy (vector<Point2d> cloud, double& min_x, double& min_y, double& max_x, double& max_y){
    min_x = limit::max();
    min_y = limit::max();
    max_x = limit::min();
    max_y = limit::min();
    for(Point2d point : cloud){
        if(point.x < min_x){
            min_x = point.x;
        } else if (point.x > max_x){
            max_x = point.x;
        }
        if(point.y < min_y){
            min_y = point.y;
        } else if (point.y > max_y){
            max_y = point.y;
        }
    }
}

void draw_point(Mat img, Point2d point, double ratio, int line_thickness, const Scalar &color){
    int cross_size = 4;
    int size = img.size().height;

    Point2d line_ud_start(point.x * ratio - cross_size, size - (point.y * ratio - cross_size));
    Point2d line_ud_end(point.x * ratio + cross_size, size - (point.y * ratio + cross_size));

    Point2d line_du_start(point.x * ratio - cross_size, size - (point.y * ratio + cross_size));
    Point2d line_du_end(point.x * ratio + cross_size, size - (point.y * ratio - cross_size));

    line(img, line_ud_start, line_ud_end, color, line_thickness);
    line(img, line_du_start, line_du_end, color, line_thickness);
}

Mat Ransac::show_line(Line l, bool is_result){
    int size = 1024;
    string name;
    if(is_result){
        name = "Result: " + to_string(l.a) + "x +" + to_string(l.b);
    }else{
        name = "Guess";
    }

    int line_thickness_cs = 2;
    int line_thickness_points = 2;
    int line_thickness_result = 3;
    Scalar color_bg = Scalar::all(255);
    Scalar color_cs = Scalar::all(0);
    Scalar color_points = Scalar::all(0);
    Scalar color_result = is_result ? Scalar(0, 255, 0) : Scalar(255, 0, 0);

    Mat img(size, size, CV_8UC3, color_bg);

    double min_x, min_y, max_x, max_y;
    min_max_xy(_points, min_x, min_y, max_x, max_y);

    /*
    if(max_x <= 0 && max_x > -1) max_x = 1;
    if(max_y <= 0 && max_y > -1) max_y = 1;
    if(min_x >= 0 && min_x < 1) min_x = -1;
    if(min_y >= 0 && min_y < 1) min_y = -1;
*/
    cout << "D=[" << min_x << ";" << max_x << "], W=[" << min_y << ";" << max_y << "]" << endl;

    double x_dist = abs(min_x) + abs(max_x);
    double y_dist = abs(min_y) + abs(max_y);

    double ratio = abs(size/max(x_dist, y_dist));

    double zero_line_x = ratio * abs(min_x);
    double zero_line_y = size - ratio * abs(min_y);

    arrowedLine(img, Point2d(zero_line_x, size), Point2d(zero_line_x, 0), color_cs, line_thickness_cs, 8, 0, 0.01);
    arrowedLine(img, Point2d(0, zero_line_y), Point2d(size, zero_line_y), color_cs, line_thickness_cs, 8, 0, 0.01);

    Point2d offset = Point2d(abs(min_x), abs(min_y));

    for(const Point2d &point: this->_points){
        draw_point(img, point + offset, ratio, line_thickness_points, color_points);
    }

    Point2d   result_start(min_x * ratio,
                         size - get_y(l, min_x) * ratio),
            result_end(max_x * ratio,
                       size - get_y(l, max_x) * ratio);

    line(img, result_start + offset, result_end + offset, color_result, line_thickness_result);

    string text;
    if (is_result) {
        text = "Result after " + to_string(guess_num-1) + " iterations";
    } else {
        text = "Iteration Nr.: " + to_string(guess_num);
    }

    int font_face = 0;
    double font_scale = .5;
    int thickness = 1;
    int *baseline = nullptr;

    Size text_size = getTextSize(text, font_face, font_scale, thickness, baseline);
    putText(img, text, Point2d(size - text_size.width - 5, 1024 - text_size.height - 5), font_face, font_scale, Scalar::all(0), thickness);

    imshow(name, img);
    return img;
}

void Ransac::show_result(Line result, string result_path){
    Mat res = show_line(result, true);
    if(strcmp(result_path.c_str(), "") != 0){
        imwrite(result_path, res);
    }
    while (waitKey(1) != 27);
}


