#include <fstream>
#include <string>
#include <iostream>
#include <sstream>

#include "Ransac.hpp"

#define PATH_DEFAULT            "./data.csv"
#define MAX_ITER_DEFAULT        1000
#define CONSENS_EDGE_DEFAULT    1.0
#define ACCURACY_DEFAULT        90
#define SHOW_PROGRESS_DEFAULT   false
#define PROGRESS_DELAY_DEFAULT  10
#define SAVE_RESULT_DEFAULT     "./result.png"

int interpret_input_params(int argc, char* argv[]);
vector<Point2d> read_data(const string &path);
void help(const string &program_name);

using namespace std;

string path =  PATH_DEFAULT;
int max_iter = MAX_ITER_DEFAULT;
double consens_edge = CONSENS_EDGE_DEFAULT;
double accuracy = ACCURACY_DEFAULT;
bool show_progress = SHOW_PROGRESS_DEFAULT;
int progress_delay = PROGRESS_DELAY_DEFAULT;
string result_path = SAVE_RESULT_DEFAULT;

int main(int argc, char* argv[]){
    if(interpret_input_params(argc, argv) != 0){
        return 1;
    }
    Ransac ran;

    Line result = ran.approx(read_data(path), max_iter, consens_edge, accuracy, show_progress, progress_delay);

    ran.show_result(result, result_path);

    cout << "Result: y = " << result.a << "x + " << result.b << endl;

    return 0;
}

bool streq(const char* str_1, const char* str_2){
    return strcmp(str_1, str_2) == 0;
}

int interpret_input_params(int argc, char* argv[]){
    for(int i = 1; i < argc; i++) {
        if (streq(argv[i], "--data") || streq(argv[i], "-d")){
            path = argv[i+1];
            i++;
        } else if (streq(argv[i], "--max-iterations") || streq(argv[i], "-i")){
            max_iter = atoi(argv[i+1]);
            i++;
        } else if (streq(argv[i], "--consens-edge") || streq(argv[i], "-c")){
            consens_edge = atof(argv[i+1]);
            i++;
        } else if (streq(argv[i], "--accuracy") || streq(argv[i], "-a")){
            accuracy = atof(argv[i+1]);
            i++;
        } else if (streq(argv[i], "--progress") || streq(argv[i], "-p")) {
            show_progress = streq(argv[i + 1], "on");
            i++;
        } else if (streq(argv[i], "--progress-delay") || streq(argv[i], "-pd")) {
            progress_delay = atoi(argv[i + 1]);
            show_progress = true;
            i++;
        } else if (streq(argv[i], "--save-result") || streq(argv[i], "-s")) {
            result_path = argv[i + 1];
            i++;
        }else if (streq(argv[i], "--help") || streq(argv[i], "-h")) {
            help(argv[0]);
            return 1;
        } else {
            cout << "Wrong argument: " << argv[i] << "\nCall with --help / -h for more information." << endl;
            return 1;
        }
    }

    return 0;
}

vector<Point2d> read_data(const string &path){
    ifstream input(path);
    vector<Point2d> points;
    string line;
    string token;
    double x = 0, y = 0;

    while(getline(input, line)){
        std::istringstream ss(line);
        std::getline(ss, token, ',');
        x = atof(token.c_str());
        std::getline(ss, token, ',');
        y = atof(token.c_str());
	    points.emplace_back(x, y);
	    cout << points.at(points.size()-1) << endl;
    }

    return points;
}

void help(const string &program_name) {
    cout << "Parametrization like such:\n" <<
         " --data | -d" <<              "\t\t<Path to *.csv file with points> (default: " << PATH_DEFAULT << ")\n" <<
         " --max-iterations | -i" <<    "\t<# of max iterations> (default: " << MAX_ITER_DEFAULT << ")\n" <<
         " --consens-edge | -c" <<      "\t<distance of consens edge> (default: " << CONSENS_EDGE_DEFAULT << ")\n" <<
         " --accuracy | -a" <<          "\t<Accuracy percentage> (default: " << ACCURACY_DEFAULT << ")\n" <<
         " --progress | -p" <<          "\t<on/off> (default: " << (SHOW_PROGRESS_DEFAULT ? "on" : "off") << ")\n" <<
         " --progress-delay | -pd" <<   "\t<ms between single iterations> (default: " << PROGRESS_DELAY_DEFAULT << ")\n" <<
         " --save-result | -s" <<       "\t<path at which to save the result> (default: " << SAVE_RESULT_DEFAULT << ")\n" <<
         "\nExample: " << program_name << " -d /path/to/points.csv -i 200 -c 0.5 -a 50 -p on" << endl;
}
