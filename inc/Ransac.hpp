#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include <thread>
#include <vector>
#include <limits>
#include <random>
#include <iostream>

using namespace std;
using namespace cv;

typedef numeric_limits<double> limit; 

/// Struct Representing a line in 2D Space
struct Line{
    double a, b;
};

/// Struct used to return two values at once
struct Confidant{
    double mse, accur;
};

/// Class to solve the linear regression problem using the RANSAC algorithm
class Ransac{
	private:
    /// Points to use for estimation
    vector<Point2d> _points;

    int guess_num;
    /// Method to draw a coordinate system with all points and the given line
    /// \param l line to draw
    /// \param is_result draw line a different color if the line is the result
    /// \return image of drawn line in coordinate system
    Mat show_line(Line l, bool is_result);

    // ==== Helper Methods ==== //
    /// Method computes Mean Squared Error and Accuracy of the given line and returns both on form of a Confident struct
    /// \param cloud Point cloud to compute the error for
    /// \param line Line to compute the error with
    /// \param consens_edge consens edge distance to check if points are inside
    /// \return struct that consists of Mean Squared Error and Accuracy for given line
    Confidant get_confidant(vector<Point2d> cloud, Line line, double consens_edge);
    /// Method computes y coordinate on line x coordinate
    /// \param line Line to compute the y coordinate with
    /// \param x x coordinate to compute the y coordinate for
    /// \return y at location x on line
    double inline get_y(Line line, double x);
    /// Method computes the line that connects a and b
    /// \param a First Point
    /// \param b Second Point
    /// \return Line that connects a and b
    Line get_line(Point2d a, Point2d b);

	public:

    explicit Ransac();

    /// Method computed the line that best fits the given point cloud using the RANSAC algorithm
    /// \param points Points to work with
    /// \param max_iter maximum number of iterations
    /// \param consens_edge absolute y difference between line and point must be smaller than that
    /// \param accuracy defines how many points have to be inside the consens_edge
    /// \param show_progress display all tested lines while searching
    /// \return Best approximation for given data using the RANSAC algorithm
    Line approx(vector<Point2d> points, int max_iter = 1000, double consens_edge = 1, double accuracy = 90,  bool show_progress = false, int progress_delay = 10);

    /// Method displays result to user
    /// \param result to display
    /// \param save_path path to save result image (leave empty if ot should not be saved)
    /// \return image of result
    void show_result(Line result, string result_path = "");

};
